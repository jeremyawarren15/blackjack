﻿﻿using System;
using System.Collections.Generic;
using PlayerDetail;
using PlayMaterial;
using System.Threading;

namespace blackjack
{
    class Program
    {
        public static List<Player> Players = new List<Player>();
        public static Deck Deck = new Deck();
        public static List<Card> Discard = new List<Card>();
        public static Hand DealerHand = new Hand();
        public static bool ShowDealerDownCard = false;

        static void Main()
        {
            
            GetPlayers();

            while (true)
            {
				PlaceBets();
				DealOut();
				PlayerCycle();

                Console.Write($"\nWould you like to play again? (Y or N)");
                var response = Console.ReadKey();
                if (response.Key == ConsoleKey.N)
                {
                    break;
                }
                ResetGame();
            }
        }

        private static void GetPlayers()
        {
            var playerCount = 1;
            while (true)
            {
                Console.Clear();
                Console.Write($"What is your name, Player {playerCount}? ");
                var name = Console.ReadLine();

                Console.Clear();
                Console.Write("How much money do you have? ");
                var cash = Convert.ToInt32(Console.ReadLine());

                Players.Add(new Player(name, cash));

                Console.Clear();
                Console.Write("Are there any more players? (Y or N)");
                var resp = Console.ReadKey();
                playerCount++;
                if (resp.Key == ConsoleKey.N)
                {
                    break;
                }
            }
        }

        private static void Deal(Player player)
        {
            player.AddCard(Deck.Draw());

        }

        private static void DealOut()
        {
            int dealDelay = 1000;
            for (int i = 0; i < 2; i++)
            {
                foreach (Player p in Players)
                {
                    Console.Clear();
                    if (Deck.Cards.Count == 0)
                    {
                        RecycleDeck();
                    }
                    Deal(p);
                    DrawTable();
                    Thread.Sleep(dealDelay);
                }

                // Deal to the dealer
                ShowDealerDownCard = false;
                DealerHand.AddCard(Deck.Draw());
                Console.Clear();
                DrawTable();
                Thread.Sleep(dealDelay);
            }
        }

        private static void PlayerCycle()
        {
            foreach (Player p in Players)
            {
                while (p.Cards.Value <= 21)
                {
                    Console.Clear();
                    DrawTable();
                    Console.WriteLine($"\n{p.Name}: (H)it or (S)tand?");

                    var response = Console.ReadKey();
                    if (response.Key == ConsoleKey.H)
                    {
                        Deal(p);
                    }
                    else if (response.Key == ConsoleKey.S)
                    {
                        break;
                    }

                    if (p.Cards.Value > 21 &&
                        p.Cards.Contains(Rank.Ace))
                    {
                        p.Cards.GetFirst(Rank.Ace).AceDowngrade();
					}
                }
            }

            ShowDealerDownCard = true;
            Console.Clear();
            DrawTable();

            var winner = new Player("Dealer", 0);
            foreach (Player p in Players)
            {
                if (p.Cards.Value > DealerHand.Value &&
                    p.Cards.Value <= 21)
                {
                    Console.Clear();
                    DrawTable();
                    Console.Write($"\n{p.Name} wins!");
                    p.AddCash(p.Bet * 2);
                }
                else {
                    Console.Clear();
                    DrawTable();
                    Console.Write($"Dealer wins.");
                }
            }

            RemoveBrokePlayers();
        }

        private static void DrawTable()
        {
            var count = 1;
            Console.Write("Dealer\t");
            if (!ShowDealerDownCard)
            {
                if (DealerHand.Cards.Count > 0)
                {
                    PrintCard(DealerHand.Cards[0]);
				}
            }
            else 
            {
                foreach (Card c in DealerHand)
                {
                    PrintCard(c);
                    Console.Write(" ");
                }
            }
            Console.Write("\n");

            foreach (Player p in Players)
            {
                Console.Write($"{p.Name}\t");
                foreach (Card c in p.Cards)
                {
                    PrintCard(c);
                    Console.Write(" ");
                }
                count++;
                Console.Write($"\t${p.Cash}");
                Console.WriteLine();
            }
        }

        private static void PrintCard(Card card)
        {
            Console.BackgroundColor = ConsoleColor.White;
            if (card.Suit == Suit.Diamond ||
                card.Suit == Suit.Heart)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Black;
            }

            switch (card.Rank)
            {
                case Rank.Ace:
                    Console.Write("A");
                    break;
                case Rank.Jack:
                    Console.Write("J");
                    break;
                case Rank.Queen:
                    Console.Write("Q");
                    break;
                case Rank.King:
                    Console.Write("K");
                    break;
                case Rank.Two:
                    Console.Write("2");
                    break;
                case Rank.Three:
                    Console.Write("3");
                    break;
                case Rank.Four:
                    Console.Write("4");
                    break;
                case Rank.Five:
                    Console.Write("5");
                    break;
                case Rank.Six:
                    Console.Write("6");
                    break;
                case Rank.Seven:
                    Console.Write("7");
                    break;
                case Rank.Eight:
                    Console.Write("8");
                    break;
                case Rank.Nine:
                    Console.Write("9");
                    break;
                case Rank.Ten:
                    Console.Write("10");
                    break;
                default:
                    Console.Write(card.Rank);
                    break;
            }

            switch (card.Suit)
            {
                case Suit.Diamond:
                    Console.Write("♦");
                    break;
                case Suit.Heart:
                    Console.Write("♥");
                    break;
                case Suit.Club:
                    Console.Write("♣");
                    break;
                case Suit.Spade:
                    Console.Write("♣");
                    break;
                default:
                    Console.Write("?");
                    break;
            }
            Console.ResetColor();
        }

        private static void PlaceBets()
        {
            foreach (Player p in Players)
            {
                int bet;
                while (true)
                {
                    Console.Clear();
                    DrawTable();
                    Console.Write($"\nWhat is your bet, {p.Name}? ");
                    bet = Convert.ToInt32(Console.ReadLine());
                    if (bet <= p.Cash)
                    {
                        p.Bet = bet;
                        p.Cash -= bet;
                        break;
                    }
                    else
                    {
                        Console.WriteLine($"Bet cannot exceed amount {p.Cash}");
                        Console.WriteLine("Press enter.");
                        Console.ReadKey();
                    }
                }
            }
        }

        private static void ResetGame()
        {
            // Adds each card to discard pile and clears all cards from each
            // players hand
            foreach (Player p in Players)
            {
                Discard.AddRange(p.Cards.Cards);
                p.Cards.Clear();
            }
            Discard.AddRange(DealerHand.Cards);
            DealerHand.Clear();
        }

        private static void RemoveBrokePlayers()
        {
            Players.RemoveAll(p => p.Cash <= 0);
        }

        private static void RecycleDeck()
        {
            Deck.Cards.AddRange(Discard);
            Deck.Shuffle();
            Discard.Clear();
        }
    }
}