﻿using System;
using PlayMaterial;

namespace PlayerDetail
{
    public class Player
    {
        public Hand Cards= new Hand();
        public string Name { set; get; }
        public int Cash { set; get; }
        public int Bet { set; get; }

        public Player(string name, int startingCash)
        {
            Name = name;
            this.Cash = startingCash;
            this.Bet = 0;
        }

        public void AddCash(int cash)
        {
            Cash += cash;
        }

        public void GiveCash(int cash)
        {
            Cash -= cash;
        }

        public void AddCard(Card card)
        {
            Cards.AddCard(card);
        }

    }
}