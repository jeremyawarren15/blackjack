﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace PlayMaterial
{
    public enum Suit
    {
        Heart,
        Diamond,
        Spade,
        Club
    }

    public enum Rank
    {
        Ace,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King
    }

    public class Card
    {
        public Rank Rank { get; }
        public Suit Suit { get; }
        public int Value
        {
            get
            {
                if (Rank == Rank.Ace)
                {
                    if (AceUpgraded) return 11;
                    else return 1;
                }
                else if (Rank == Rank.Jack ||
                         Rank == Rank.Queen ||
                         Rank == Rank.King)
                {
                    return 10;
                }
                else
                {
                    return (int)Rank + 1;
                }
            }
        }
        public bool AceUpgraded { set; get; }

        public Card(Rank rank, Suit suit)
        {
            this.Rank = rank;
            this.Suit = suit;
            this.AceUpgraded = true;
        }

        public void AceDowngrade()
        {
            AceUpgraded = false;
        }

        public override string ToString()
        {
            return string.Format("{0} of {1}s", this.Rank, this.Suit);
        }
    }

    public class Deck
    {
        public List<Card> Cards { set; get; }

        public Deck()
        {
            Cards = new List<Card>(); 
            GenerateDeck();
            Shuffle();
        }

        public void GenerateDeck()
        {
            Cards.Clear();
            foreach (Suit s in Enum.GetValues(typeof(Suit)))
            {
                foreach (Rank r in Enum.GetValues(typeof(Rank)))
                {
                    Cards.Add(new Card(r, s));
                }
            }
        }

        public Card Draw()
        {
            var card = Cards[0];
            Cards.RemoveAt(0);
            return card;
        }

        public Card GetCardInDeck(int index)
        {
            return Cards[index];
        }

        public void Shuffle()
        {
            var rnd = new Random();
            var c = Cards.Count;
            var TempList = new List<Card>(Cards);
            Cards.Clear();
            for (int i = 0; i < c; i++)
            {
                int r = rnd.Next(TempList.Count);
                Cards.Add(TempList[r]);
                TempList.RemoveAt(r);
            }
        }

    }

    public class Hand : IList
    {
        public List<Card> Cards { set; get; }
        public int Value 
        {
            get
            {
				var count = 0;
				foreach (Card c in Cards)
				{
					count += c.Value;
				}
				return count;
            }
			set
			{
                value = Value;
            }
        }
        public bool IsFixedSize => ((IList)Cards).IsFixedSize;
        public bool IsReadOnly => ((IList)Cards).IsReadOnly;
        public int Count => ((IList)Cards).Count;
        public bool IsSynchronized => ((IList)Cards).IsSynchronized;
        public object SyncRoot => ((IList)Cards).SyncRoot;
        public object this[int index] { get => ((IList)Cards)[index]; set => ((IList)Cards)[index] = value; }

        public Hand()
        {
            this.Cards = new List<Card>();
            this.Value = 0;
        }

        public void AddCard(Card card)
        {
            Cards.Add(card);
            Value += card.Value;
        }

        public bool IsBlackjack()
        {
            if (Cards[0].Rank == Rank.Ace ||
            Cards[0].Value == 10)
            {
                if (Cards[0].Value + Cards[1].Value == 21)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool DidBust()
        {
            if (Value > 21)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Add(object value)
        {
            return ((IList)Cards).Add(value);
        }

        public void Clear()
        {
            ((IList)Cards).Clear();
        }

        public bool Contains(object value)
        {
            return ((IList)Cards).Contains(value);
        }

        public bool Contains(Rank rank)
        {
            foreach (Card c in Cards)
            {
                if (c.Rank == rank)
                {
                    return true;
                }
            }
            return false;
        }

        public Card GetFirst(Rank rank)
        {
            foreach (Card c in Cards)
            {
                if (c.Rank == rank)
                {
                    return c;
                }
            }
            // I don't know if there is a way to overcome this somehow
            // This might just be a consequence that I have to deal with
            return Cards[0];
        }

        public int IndexOf(object value)
        {
            return ((IList)Cards).IndexOf(value);
        }

        public void Insert(int index, object value)
        {
            ((IList)Cards).Insert(index, value);
        }

        public void Remove(object value)
        {
            ((IList)Cards).Remove(value);
        }

        public void RemoveAt(int index)
        {
            ((IList)Cards).RemoveAt(index);
        }

        public void CopyTo(Array array, int index)
        {
            ((IList)Cards).CopyTo(array, index);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IList)Cards).GetEnumerator();
        }
    }
}